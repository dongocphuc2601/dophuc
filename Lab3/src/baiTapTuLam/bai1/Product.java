package baiTapTuLam.bai1;

public class Product {
	public String IDPro;
	public String namePro;
	
	public Product() {
		super();
	}
	public Product(String iDPro, String namePro) {
		super();
		IDPro = iDPro;
		this.namePro = namePro;
	}
	public String getIDPro() {
		return IDPro;
	}
	public void setIDPro(String iDPro) {
		IDPro = iDPro;
	}
	public String getNamePro() {
		return namePro;
	}
	public void setNamePro(String namePro) {
		this.namePro = namePro;
	}
	@Override
	public String toString() {
		return "Product [IDPro=" + IDPro + ", namePro=" + namePro + "]";
	}
}
