package baiTapTuLam.bai1;

import java.util.Scanner;

public class DrinkingList extends Drinking{
	public DrinkingList(String iDPro, String namePro, String unit, double price) {
		super(iDPro, namePro, unit, price);
		// TODO Auto-generated constructor stub
	}
	public static Scanner sc = new Scanner(System.in);
	public static String IDPro;
	public static String namePro;
	public static String unit;
	public static boolean checkValid = false;
	public static boolean checkValid1 = false;
	public static double rateDrink ;
	public void addDrink() {
		do {
			System.out.print("Input ID of product(The Id Product format \"HHXXX\"; X is a number): ");
			IDPro = sc.nextLine().toUpperCase().trim();
			checkValid = IDPro.matches("^[H]{2}\\d{3}$");
			if (!checkValid) {
				System.out.println("The id Product is not match with the the original form! Please input again!");
			}
		} while (!checkValid);
		this.setIDPro(IDPro);
		do {
			System.out.print("Input name of product(The Product's Name doesn't have the number): ");
			namePro = sc.nextLine();
			checkValid1 = namePro.matches("[A-Za-z_\\s]+");
			if (!checkValid1) {
				System.out.println("The id Product is not match with the the original form! Please input again!");
			}
		} while (!checkValid1);
		this.setNamePro(namePro);
		do {
			System.out.print("Input name of unit(The Product's Name doesn't have the number): ");
			namePro = sc.nextLine();
			checkValid1 = namePro.matches("[A-Za-z_\\s]+");
			if (!checkValid1) {
				System.out.println("The id Product is not match with the the original form! Please input again!");
			}
		} while (!checkValid1);
		this.setNamePro(namePro);
		do {
			System.out.print("Input the price of drinhking water: ");
			price = sc.nextInt();
			if (price < 0) {
				System.out.print("The price is not valid! Please input again!");
			}
		} while (price < 0);
		this.setPrice(price);
		do {
			System.out.print("Input the price of drinhking water: ");
			rateDrink = sc.nextDouble();
			if (rateDrink < 0) {
				System.out.print("The rate of drinking is not valid! Please input again!");
			}
		} while (rateDrink < 0);
		this.getTotal(rateDrink);
	}
}
