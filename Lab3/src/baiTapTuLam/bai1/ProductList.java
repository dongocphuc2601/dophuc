package baiTapTuLam.bai1;

import java.util.Scanner;

public class ProductList extends Product{
	public ProductList(String iDPro, String namePro) {
		super(iDPro, namePro);
		// TODO Auto-generated constructor stub
	}

	public static Scanner sc = new Scanner(System.in);
	public static String IDPro;
	public static String namePro;
	public static boolean checkValid = false;
	public static boolean checkValid1 = false;
	
	public void addPro() {
		do {
			System.out.print("Input ID of product(The Id Product format \"HHXXX\"; X is a number): ");
			IDPro = sc.nextLine().toUpperCase().trim();
			checkValid = IDPro.matches("^[H]{2}\\d{3}$");
			if (!checkValid) {
				System.out.println("The id Product is not match with the the original form! Please input again!");
			}
		} while (!checkValid);
		this.setIDPro(IDPro);
		do {
			System.out.print("Input name of product(The Product's Name doesn't have the number): ");
			namePro = sc.nextLine();
			checkValid1 = namePro.matches("[A-Za-z_\\s]+");
			if (!checkValid1) {
				System.out.println("The id Product is not match with the the original form! Please input again!");
			}
		} while (!checkValid1);
		this.setNamePro(namePro);
	}
	
	public void printOut() {
		this.toString();
		System.out.print(toString());
	}
}