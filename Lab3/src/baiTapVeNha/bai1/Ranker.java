package baiTapVeNha.bai1;

public class Ranker extends Employee{
	public String rank;
	public int yearGet;
	public static final String test1 = "Giám Đốc";
	public static final String test2 = "trưởng phòng";
	public static final String test3 = "phó phòng";
	public Ranker(String idEmp, String nameEmp, double coefSalary, String rank, int yearGet) {
		super(idEmp, nameEmp, coefSalary);
		this.rank = rank;
		this.yearGet = yearGet;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public int getYearGet() {
		return yearGet;
	}
	public void setYearGet(int yearGet) {
		this.yearGet = yearGet;
	}
	@Override
	public String toString() {
		return "Ranker [rank=" + rank + ", yearGet=" + yearGet + ", idEmp=" + idEmp + ", nameEmp=" + nameEmp
				+ ", coefSalary=" + coefSalary + "]";
	}
	public double getIncomeRanker() {
		double hsld;
		double totalIncome;
		if (this.rank.equals(test1)) {
			hsld =7.0;
			totalIncome = this.getIncome() +(hsld*1500);
			return totalIncome;
		}else if (this.rank.equals(test2)) {
			hsld = 6.0;
			totalIncome = this.getIncome() +(hsld*1500);
			return totalIncome;
		}else if (this.rank.equals(test3)) {
			hsld = 4.5;
			totalIncome = this.getIncome() +(hsld*1500);
			return totalIncome;
		}
		return this.getIncome() +(1.0*1500);
	}
}
