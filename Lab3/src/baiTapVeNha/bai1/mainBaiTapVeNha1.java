package baiTapVeNha.bai1;

public class mainBaiTapVeNha1 {
	public static void main(String[] arr) {
		Employee emp = new Employee("NV001", "Tran Thi Dao", 2.5);
		System.out.println("The information of "+emp.getIdEmp()+":");
		System.out.println(emp.toString());
		System.out.print("Total income of "+emp.getIdEmp()+":");
		System.out.println(emp.getIncome());
		Ranker r = new Ranker("NV010", "Phan Ha Chuong", 7.0, "Giám Đốc", 10);
		System.out.println("The information of the ranker "+r.getIdEmp()+": ");
		System.out.println(r.toString());
		System.out.println("Total income of the ranker "+r.getIdEmp()+": ");
		System.out.println(r.getIncomeRanker());
		
	}
}
