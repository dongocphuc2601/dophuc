package baiTapVeNha.bai1;

public class Employee {
	public String idEmp;
	public String nameEmp;
	public double coefSalary;
	public static final double basicSalary = 1150;
	
	public Employee(String idEmp, String nameEmp, double coefSalary) {
		super();
		this.idEmp = idEmp;
		this.nameEmp = nameEmp;
		this.coefSalary = coefSalary;
	}
	public String getIdEmp() {
		return idEmp;
	}
	public void setIdEmp(String idEmp) {
		this.idEmp = idEmp;
	}
	public String getNameEmp() {
		return nameEmp;
	}
	public void setNameEmp(String nameEmp) {
		this.nameEmp = nameEmp;
	}
	public double getCoefSalary() {
		return coefSalary;
	}
	public void setCoefSalary(double coefSalary) {
		this.coefSalary = coefSalary;
	}
	@Override
	public String toString() {
		return "Employee [idEmp=" + idEmp + ", nameEmp=" + nameEmp + ", coefSalary=" + coefSalary + "]";
	}
	
	public double getIncome() {
		return this.coefSalary * this.basicSalary;
	}
}
