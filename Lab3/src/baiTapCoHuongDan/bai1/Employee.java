package baiTapCoHuongDan.bai1;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class Employee {
	public String idEmp;
	public String nameEmp;
	public int yearDo;
	public int baseSal;
	public int daysOff;
	public static final int currentYear = 2020;
	public static final int basicSalary = 1150;

	
	public Employee(String idEmp, String nameEmp, int yearDo, int baseSal, int daysOff) {
		super();
		this.idEmp = idEmp;
		this.nameEmp = nameEmp;
		this.yearDo = yearDo;
		this.baseSal = baseSal;
		this.daysOff = daysOff;
	}


	public String getIdEmp() {
		return idEmp;
	}


	public void setIdEmp(String idEmp) {
		this.idEmp = idEmp;
	}


	public String getNameEmp() {
		return nameEmp;
	}


	public void setNameEmp(String nameEmp) {
		this.nameEmp = nameEmp;
	}


	public int getBaseSal() {
		return baseSal;
	}


	public void setBaseSal(int baseSal) {
		this.baseSal = baseSal;
	}


	public int getDaysOff() {
		return daysOff;
	}


	public void setDaysOff(int daysOff) {
		this.daysOff = daysOff;
	}

	public int getYearDo() {
		return yearDo;
	}


	public void setYearDo(int yearDo) {
		this.yearDo = yearDo;
	}

	public double setPhuCap() {
		int khoangThoiGianLam = this.currentYear - this.yearDo;
		double soPhuCapThem = khoangThoiGianLam *(basicSalary/100);
		return soPhuCapThem;
	}

	public double setThiDua() {
		if (this.daysOff <= 1) {
			System.out.println("He so Thi dua: A");
			return 1.0;
		} else if (this.daysOff <= 3) {
			System.out.println("He so Thi dua: B");
			return 0.75;
		} else
			return 0.5;
	}

	public double getSalary() {
		return basicSalary * this.baseSal * setThiDua() + setPhuCap();
	}

	@Override
	public String toString() {
		return "Employee [idEmp=" + idEmp + ", nameEmp=" + nameEmp + ", yearWork=" + yearDo + ", baseSal=" + baseSal
				+ ", daysOff=" + daysOff + getSalary()+ setThiDua()+"]";
	}
	
}
