package baiTapCoHuongDan.bai1;

public class mainNhanVien1 {
	public static void main(String arr[]) {
		// input the employee's information
		Employee emp = new Employee("SE01", "Do Ngoc Phuc", 2000, 5, 4);
		System.out.println("Thong tin nhan vien:");
		System.out.print("Ho ten: "+emp.getNameEmp());
		System.out.print("MSNV: "+emp.getIdEmp());
		System.out.print("The year of beginning working: "+emp.getYearDo());
		System.out.print("Basic salary number: "+emp.getBaseSal());
		System.out.print("Employee's Day Off: "+emp.getDaysOff());
		System.out.println("Employee's salary: "+emp.getSalary());
		// input the manager's information
		Manager mn = new Manager("SF01", "Tran Chien", 2000, 10, 1, "Leader", "Human Management");
		System.out.print(mn.phuCapLanhDao());
	}
}
