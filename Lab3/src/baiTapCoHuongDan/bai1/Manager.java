package baiTapCoHuongDan.bai1;

import java.util.Scanner;

public class Manager extends Employee{
	public static Scanner sc = new Scanner(System.in);
	public String Rank;
	public String department;
	public static final double heSoPhuCapLD1 = 5.0;
	
	
	
	public Manager(String idEmp, String nameEmp, int yearDo, int baseSal, int daysOff, String rank, String department) {
		super(idEmp, nameEmp, yearDo, baseSal, daysOff);
		Rank = rank;
		this.department = department;
	}


	public String getRank() {
		return Rank;
	}


	public void setRank(String rank) {
		Rank = rank;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public double phuCapLanhDao() {
		double heSoPhuCapLD;
		System.out.print("He so phu cap cua Lanh Dao: ");
		heSoPhuCapLD = sc.nextDouble();
		double phuCap = Manager.this.basicSalary* heSoPhuCapLD;
//		System.out.print("Phu Cap Lanh Dao: "+phuCap);
		return phuCap;
	}


	@Override
	public String toString() {
		return "Manager [Rank=" + Rank + ", department=" + department + ", idEmp=" + idEmp + ", nameEmp=" + nameEmp
				+ ", yearDo=" + yearDo + ", baseSal=" + baseSal + ", daysOff=" + daysOff + "]";
	}
	
}
