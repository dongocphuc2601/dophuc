package hw;

import java.util.Scanner;

public class lamCoHuongDan {
	public static boolean checkValid;
	public static Scanner sc = new Scanner(System.in);

	// 9
	public static void name(long n) {
		if (n % 2 == 0) {
			System.out.print("");
		}
	}

	// 8
	public static void cau8() {
		int n = 0;
		do {
			System.out.print("Input n: ");
			n = sc.nextInt();
			if (n < 0) {
				System.out.println(n + " isn't the plus integer!");
			}
		} while (n < 0);
		System.out.println(n + " is the plus integer!");
	}

	// 7. Đếm xem số nguyên dương n có bao nhiêu chữ số. Ví dụ: n = 315 -> có 3 chữ
	// số
	public static long cau7(long n) {
		long d;
		long sum = 0;
		while (n > 0) {
			d = n % 10;
			n = n / 10;
			sum++;
			cau7(n);
		}
//		System.out.println("So cua day: " + sum);
		return sum;
	}

	// 6
	public static boolean isPrimeNumber(int n) {
		if (n < 2) {
			return false;
		}
		int squareRoot = (int) Math.sqrt(n);
		for (int i = 2; i <= squareRoot; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	// 5 -- ko chay dc
	public static long cau5(long n) {
		if (n == 1) {
			return 1;
		}
		return (long) (Math.pow(n, 2) + Math.pow(n - 1, 2));
	}

	// 4
	public static void cau4(long n) {
		long count = 0;
		long sum = 0;
		while (count < n) {
			if (count % 2 == 0) {
				sum += count;
			}
			count++;
		}
		System.out.println("Sum: " + sum);
	}

	// 3
	public static void cau3(long n) {
		long count = 0;
		while (count < n) {
			if (count % 2 == 0) {
				System.out.println(count + " ");
			}
			count++;
		}
	}

	// 2
	public static void cau2(long a, long b) {
		if (a > b) {
			System.out.println(a);
		} else {
			System.out.println(b);
		}
	}

	// 1
	public static void hpt2(long a, long b, long c) {
		if (a == 0) {
			System.out.println("nghiem cua phuong trinh la" + (-c) / b);
		} else {
			long d = b * b - 4 * a * c;
			if (d < 0) {
				System.out.println("Phuong Trinh Vo Nghiem!");
			} else if (d == 0) {
				System.out.println("Nghiem kep: " + (-b) / (2 * a));
			} else {
				System.out.println("Nghiem x1: " + (-b + Math.sqrt(d)) / (2 * a));
				System.out.println("Nghiem x2: " + (-b - Math.sqrt(d)) / (2 * a));
			}
		}
	}

	public static void main(String[] args) {
		int n;
//		System.out.print("Input n: ");
//		n = sc.nextInt();
//		cau3(8);
//		cau4(8);
//		cau5(5);
//		cau7(789);
		cau8();
		hpt2(2, -5, 3);
	}
}
