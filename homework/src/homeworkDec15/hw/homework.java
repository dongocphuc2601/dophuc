package hw;

import java.util.Scanner;

public class homework {
	public static Scanner sc = new Scanner(System.in);

	// 9
	public static void cau9(long n) {
		long count = 0;
		if (count < n) {
			System.out.print(count + " ");
			count++;
			cau9(n);
		}
	}

	// 8 -- em chua suy nghi ra
	public static void cau8(long n) {
		long sum = 0;
		for (int i = 1; i < n; i++) {
			sum +=1;
	  		if (sum >= i) {
				System.out.print(i);
				break;
			}
		}
	}

	// 7
	public static void cau7(long n) {
		long d = 0;
		long max = 0;
		while (n > 0) {

			d = n % 10;
			n = n / 10;
			if (d > max) {
				max = d;
			}

		}
		System.out.print(max);
	}

	// 6
	public static void cau6() {
		int n = 0;
		do {
			System.out.print("input n: ");
			n = sc.nextInt();
			if (n < 0) {
				System.out.print("The inputted number isn't negative!");
			}
		} while (n < 0);
		System.out.print("Day so chinh phuong:");
		for (int i = 0; i < n; i++) {
			double d = Math.sqrt(i);
			if (i == d * d) {
				System.out.print(i + " ");
			}
		}
	}

	// 5
	public static void cau5(double n) {
		double d = Math.sqrt(n);
		if (n == d * d) {
			System.out.println(n + " la so chinh phuong!");
		} else
			System.out.println(n + " khong la so chinh phuong!");
	}

	// 4
	public static long cau4(long n) {
		if (n == 1) {
			return 1;
		}
		if (n == 2) {
			return 2;
		}
		return n + cau4(n - 2);
	}

	// 3
	public static long cau3(long n) {
		if (n == 1) {
			return 1;
		}
		return 1 + 1 / ((cau3(n - 1)) * (cau3(n - 1)));
	}

	// 2
	public static long cau2(long n) {
		if (n == 1 || n == 0) {
			return 1;
		}
		return n * cau2(n - 1);
	}

	// 1
	public static void cau1(long a, long b, long c) {
		if (a < b && a < c) {
			System.out.println(a);
		} else if (b < a && b < c) {
			System.out.println(b);
		} else {
			System.out.println(c);
		}

	}

	public static void main(String[] arr) {
//		cau7(1345);
//		cau1(12, 10,8);
//		System.out.println(cau2(8));
//		cau3(9);
//		cau4(15);
//		cau5(7);
//		cau6();
//		cau7(99);
		cau8(22);
//		cau9(13);
	}
}
