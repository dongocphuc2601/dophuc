package baiHinhChuNhatInterface;

public abstract class Rectanglen {
	protected float dai;
	protected float rong;
	public Rectanglen(float dai, float rong) {
		super();
		this.dai = dai;
		this.rong = rong;
	}
	abstract float getDienTich();
	
	abstract float getChuVi();
	public float getDai() {
		return dai;
	}
	public void setDai(float dai) {
		this.dai = dai;
	}
	public float getRong() {
		return rong;
	}
	public void setRong(float rong) {
		this.rong = rong;
	}
}
