package baiHinhChuNhatInterface;

public class Rec2 {
	public float dai;
	public float rong;
	public Rec2(float dai, float rong) {
		super();
		this.dai = dai;
		this.rong = rong;
	}
	public float getDai() {
		return dai;
	}
	public void setDai(float dai) {
		this.dai = dai;
	}
	public float getRong() {
		return rong;
	}
	public void setRong(float rong) {
		this.rong = rong;
	}
	
}
