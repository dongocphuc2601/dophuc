package homework24Dec.moTaHinhChuNhat;

public class Rectanglen {
	public float dai;
	public float rong;
	public Rectanglen(float dai, float rong) {
		super();
		this.dai = dai;
		this.rong = rong;
	}
	public float getDai() {
		return dai;
	}
	public void setDai(float dai) {
		this.dai = dai;
	}
	public float getRong() {
		return rong;
	}
	public void setRong(float rong) {
		this.rong = rong;
	}
	
	public void dienTich(float dai, float rong) {
		float dienTich = dai * rong;
		System.out.print("Dien tich hinh chu nhat: "+dienTich);
	}
	
	public void chuVi(float dai, float rong) {
		float chuVi = (dai + rong) * 2;
		System.out.print("Chu vi hinh chu nhat: "+chuVi);
	}
}
