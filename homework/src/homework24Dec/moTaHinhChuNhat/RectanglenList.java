package homework24Dec.moTaHinhChuNhat;

import java.util.Scanner;

public class RectanglenList extends Rectanglen {
	public static Scanner sc = new Scanner(System.in);
	
	public RectanglenList(float dai, float rong) {
		super(dai, rong);
		// TODO Auto-generated constructor stub
	}

	public void input() {
		float dai = 0;
		float rong = 0;
		do {
			System.out.println("Nhap Chieu dai: ");
			dai = sc.nextFloat();
		} while (dai < 0);
		
		do {
			System.out.println("Nhap Chieu rong: ");
			rong = sc.nextFloat();
		} while (rong < 0);
		
		super.setDai(dai);
		super.setRong(rong);
		super.chuVi(dai, rong);
		super.dienTich(dai, rong);
	}
}
