package homeworkDec19.baiTapTuLam.bai1;

import java.util.Scanner;

public class ProductList {
	public static Scanner sc = new Scanner(System.in);
	public static String unit;
	public static String namePro;
	public static int quantity;
	public static String price1;
	public static float price;
	public static float VAT;
	public static boolean checkVaild;
	Product pr = new Product();

	public void addProduct() {
		System.out.println("Input NAME Product: ");
		namePro = sc.nextLine();
		do {
			System.out.println("Input UNIT Product: ");
			unit = sc.nextLine().toUpperCase().trim();
			checkVaild = unit.matches("^[a-zA-Z]{3,}$");
			if (!checkVaild) {
				System.out.println("The input just has the alphabet!");
			}
			if (!unit.equalsIgnoreCase("THUNG")) {
				System.out.println("The input unit is \"THUNG\", \"KET\", \"LON\", \"CHAI\" ");
			}
		} while (!checkVaild /*|| !unit.equalsIgnoreCase("THUNG") || !unit.equalsIgnoreCase("LON")
				|| !unit.equalsIgnoreCase("CHAI") || !unit.equalsIgnoreCase("KET")*/);

		do {
			try {
				System.out.println("Input Price Product: ");
				price1 = sc.nextLine().trim();
				checkVaild = price1.matches("^\\d{1,}$");

				price = Float.parseFloat(price1);
				if (!checkVaild) {
					System.out.println("The price just has number! Please check again!");
				}
				if (price < 0) {
					System.out.println("The price is not valid! Please check again!");
				}
			} catch (Exception e) {
				System.out.println("The price just has number!");
			}

		} while (price < 0);

		do {
			System.out.println("Input quantities of Product: ");
			quantity = sc.nextInt();
			if (quantity < 0) {
				System.out.println("The quantities of product is not valid! Please check again!");
			}
		} while (quantity < 0);

		do {
			System.out.println("Input VAT of Product: ");
			VAT = sc.nextFloat();
			if (VAT < 0) {
				System.out.println("The VAT is not valid! Please check again!");
			}
		} while (VAT < 0);
		pr.setNameOfProduct(namePro);

		pr.setUnit(unit);
//		System.out.println("");
		pr.setPrice(price);
//		System.out.println("");
		pr.setQuantity(quantity);
//		System.out.println("");
		pr.setVAT(VAT);
//		System.out.println("");
		pr.getTotalPrice(unit);
	}
}
