package homeworkDec19.baiTapTuLam.bai1;

public class Product {
	public String nameOfProduct;
	public String unit;
	public int quantity;
	public float price;
	public float VAT;

	public Product() {

	}

	public Product(String nameOfProduct, String unit, int quantity, float price, float VAT) {
		this.nameOfProduct = nameOfProduct;
		this.unit = unit;
		this.quantity = quantity;
		this.price = price;
		this.VAT = VAT;
	}

	public String getNameOfProduct() {
		return nameOfProduct;
	}

	public void setNameOfProduct(String nameOfProduct) {
		this.nameOfProduct = nameOfProduct;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getVAT() {
		return VAT;
	}

	public void setVAT(float VAT) {
		this.VAT = VAT;
	}
	
	public float getTotalPrice(String unit) {
		float totalPrice;
		if (unit.equalsIgnoreCase("thung") || unit.equalsIgnoreCase("ket")) {
			totalPrice = this.quantity * (this.price/20) * this.VAT;
			System.out.print("Total price "+unit+":" +totalPrice);
			return totalPrice;
		}
		if (unit.equalsIgnoreCase("chai")) {
			totalPrice = this.quantity * (this.price/24) * this.VAT;
			System.out.print("Total price "+unit+":"+totalPrice);
			return totalPrice;
		}
		if(unit.equalsIgnoreCase("lon")) {
			totalPrice = this.quantity * (this.price/24) * this.VAT;
			System.out.print("Total price "+unit+":"+totalPrice);
			return totalPrice;
		}
		return 0;
	}

}
