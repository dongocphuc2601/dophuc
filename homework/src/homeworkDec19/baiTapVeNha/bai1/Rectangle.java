package homeworkDec19.baiTapVeNha.bai1;

public class Rectangle {
	public double dai;
	public double rong;
	public int n;
	public double numChange; 
	public Rectangle(double dai, double rong) {
		super();
		this.dai = dai;
		this.rong = rong;
	}
	public Rectangle() {
		
	}
	public double getDai() {
		return dai;
	}
	public void setDai(double dai) {
		this.dai = dai;
	}
	public double getRong() {
		return rong;
	}
	public void setRong(double rong) {
		this.rong = rong;
	}
	
	
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public double getNumChange() {
		return numChange;
	}
	public void setNumChange(double numChange) {
		this.numChange = numChange;
	}
	public double getDientich() {
		return this.dai*this.rong;
	}
	
	public double getChuVi() {
		return (this.dai + this.rong)*2;
	}

	public Rectangle(double dai, double rong, double numChange, int n) {
		this.dai = dai;
		this.rong = rong;
		this.numChange = numChange;
		this.n = n;		
	}
	
	public void changeSize(double dai, double rong, double numChange, int n) {
		if (n == 1) {
			System.out.print("The size of rectangle is increased: ");
			System.out.print("Dai: "+(this.dai + numChange));
			System.out.print("Rong: "+(this.rong + numChange));
		}
		if (n == 0 ) {
			System.out.print("The size of rectangle is decreased: ");
			System.out.print("Dai: "+(this.dai - this.numChange));
			System.out.print("Rong: "+(this.rong - this.numChange));
		}
	}
	
}
