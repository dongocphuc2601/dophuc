package homeworkDec19.baiTapVeNha.bai1;

import java.util.Scanner;

public class RectangleList {
	public static Scanner sc = new Scanner(System.in);
	
	public void basicProcess() {
		Rectangle r = new Rectangle(5, 6);
		System.out.print("Chu vi: "+r.getChuVi());
		System.out.println("");
		System.out.print("Dien tich: "+r.getDientich());
		System.out.println("");
		double dai;
		double rong;
		int n;
		double numberOfChange;
		System.out.print("Chieu dai: ");
		dai = sc.nextDouble();
		System.out.println("Chieu rong: ");
		rong = sc.nextDouble();
		System.out.print("Input n: ");
		n = sc.nextInt();
		System.out.print("Input numberOfChange: ");
		numberOfChange = sc.nextDouble();
		r.setDai(dai);
		r.setRong(rong);
		r.setN(n);
		r.setNumChange(numberOfChange);
		r.changeSize(r.getDai(), r.getRong(), r.getNumChange(), r.getN());
	}
}
