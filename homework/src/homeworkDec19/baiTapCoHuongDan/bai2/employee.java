package homeworkDec19.baiTapCoHuongDan.bai2;

public class employee {
	public String idEmp;
	public String name;
	public int doingDays;
	public double salary;
	public String rank;
	public employee(String idEmp, String name, int doingDays, double salary) {
		super();
		this.idEmp = idEmp;
		this.name = name;
		this.doingDays = doingDays;
		this.salary = salary;
//		this.rank = rank;
		
	}
	
	
	public String getIdEmp() {
		return idEmp;
	}
	public void setIdEmp(String idEmp) {
		this.idEmp = idEmp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDoingDays() {
		return doingDays;
	}
	public void setDoingDays(int doingDays) {
		this.doingDays = doingDays;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public double getTotalSalary(double salary) {
		return this.doingDays * this.salary;
	}
	
	public void setRank(int doingDays) {
		if (doingDays > 26) {
			this.rank = "a";
		}
		else if(doingDays < 22){
			this.rank = "c";
		}
		else this.rank = "b";

	}
	
	public double getTip(double Salary) {
		if (this.doingDays >26) {
			return getTotalSalary(Salary)*0.05;
		}
		else if(doingDays < 22) {
			return 0.0;
		}
		return getTotalSalary(Salary)*0.02;
		
	}
	
	public String toSrting() {
		return this.idEmp+", "+this.name+", "+this.doingDays+", "+this.salary;
	}
	
}
