package homeworkDec19.baiTapCoHuongDan.bai1;

public class hinhTron {
	public float banKinh;
	public static double pi =3.14;
	  
	public hinhTron() {
		super();
	}

	public hinhTron(float banKinh) {
		super();
		this.banKinh = banKinh;
	}

	public float getBanKinh() {
		return banKinh;
	}

	public void setBanKinh(float banKinh) {
		this.banKinh = banKinh;
	}
	
	public double getDienTich() {
		return pi*Math.pow(this.banKinh, 2);
	}
	
	public double getChuVi() {
		return pi*this.banKinh*2;
	}
}
