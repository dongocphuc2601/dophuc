package baiTapCoHuongDan;

import java.util.Scanner;

public class bai1 {
	public static Scanner sc = new Scanner(System.in);

	// 1
	public static void soNguyen() {
		int n = 0;
		do {
			System.out.print("Input n: ");
			n = sc.nextInt();
		} while (n < 0);
		int arr[] = new int[n];
		System.out.print("Nhap cac phan tu cho mang: ");
		for (int i = 0; i < n; i++) {
			System.out.print("Nhap phan tu cua mang " + i + ": ");
			arr[i] = sc.nextInt();
		}

		System.out.println("\nMang bat daau: ");
		for (int i = 0; i < n; i++) {
			System.out.print(arr[i] + "\t");
		}

		System.out.println("\nLiet ke cac so chan: ");
		for (int i = 0; i < n; i++) {
			if (arr[i] % 2 == 0) {
				System.out.print(arr[i] + "\t");
			}
		}

		System.out.println("Min: " + min(arr, n));
		System.out.println("Max: " + max(arr, n));
		System.out.println("Sap xep: ");
		sapxep(arr);
		xuatMang(arr, n);
		tinhTong(arr, n);
	}

	public static int min(int a[], int n) {
		int min = a[0];
		for (int i = 1; i < n; i++) {
			if (a[i] < min)
				min = a[i];
		}
		return min;
	}

	public static int max(int a[], int n) {
		int max = a[0];
		for (int i = 1; i < n; i++) {
			if (a[i] > max)
				max = a[i];
		}
		return max;
	}

	public static boolean check(int n) {
		if (n <= 1)
			return false;
		for (int i = 2; i <= Math.sqrt(n); i++)
			if (n % i == 0)
				return false;

		return true;
	}

	public static void sapxep(int arr[]) {
		int temp = arr[0];
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
				}
			}
		}
	}

	public static void xuatMang(int a[], int n) {
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + " ");
		}
	}

	public static void tinhTong(int a[], int n) {
		int sum = 0;
		for (int i = 0; i < n; i++) {
			sum += a[i];
		}
		System.out.println("Sum of Array have " + n + ": " + sum);
	}

	// them vao vi tri cuoi mang
	public static void importLastofArray(int a[], int n, int x) {
		int k = a.length + 1;
		for (int i = k; i > a.length; i++) {

		}
	}

	public static void main(String arr[]) {
		soNguyen();

	}
}
